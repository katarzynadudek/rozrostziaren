from math import floor
from random import randint

from kivy import utils
from kivy.core.text import Label
from kivy.core.window import Window
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Line, Rectangle

from Utility.Embryo import Embryo
from Utility.vonNeuman import vonNeuman


class Drawing:

    def __init__(self, wid, mesh_width, mesh_height):
        self.mesh_width = mesh_width
        self.mesh_height = mesh_height
        self.wid = wid
        self.surface = [[Embryo(x, y, 0, 0) for x in range(self.mesh_width)] for y in range(self.mesh_height)]

    def create_mesh(self):
        self.wid.canvas.clear()

        height = Window.height
        width = Window.width

        if self.mesh_height > self.mesh_width:

            stepWidth = floor((0.7 * width) / (self.mesh_height + 1))
            stepHeight = floor(0.8 * height / (self.mesh_height + 1))

        else:
            stepWidth = floor((0.7 * width) / (self.mesh_width + 1))
            stepHeight = floor(0.8 * height / (self.mesh_width + 1))

        with self.wid.canvas:
            Color(1., 1, 1)

            for index in range(0, self.mesh_height + 1):
                # poziome
                Line(points=[0.1 * width,
                             0.9 * height - stepHeight * index,
                             self.mesh_width * stepWidth + 0.1 * width,
                             0.9 * height - stepHeight * index],
                     width=1)

            for index in range(0, self.mesh_width + 1):
                # pionowe
                Line(points=[(self.mesh_width - index) * stepWidth + 0.1 * width,
                             0.9 * height,
                             (self.mesh_width - index) * stepWidth + 0.1 * width,
                             0.9 * height - (stepHeight * self.mesh_height)],
                     width=1)

        pass

    def update_step(self, embryos):

        for embryo in embryos:
            self.surface[embryo.y][embryo.x] = embryo
            self.draw_point(embryo.x, embryo.y, embryo.color)

    def draw_point(self, index_x, index_y, color):

        if self.mesh_height > self.mesh_width:

            stepWidth = floor((0.7 * Window.width) / (self.mesh_height + 1))
            stepHeight = floor(0.8 * Window.height / (self.mesh_height + 1))

        else:
            stepWidth = floor((0.7 * Window.width) / (self.mesh_width + 1))
            stepHeight = floor(0.8 * Window.height / (self.mesh_width + 1))

        with self.wid.canvas:
            color = utils.get_color_from_hex(color)
            Color(color[0], color[1], color[2], color[3])
            Rectangle(
                pos=(0.1 * Window.width + (stepWidth * index_x), 0.9 * Window.height - (stepHeight * (index_y + 1))),
                size=(stepWidth, stepWidth))




    def draw_animation(self, border_condition):

        self.surface = vonNeuman.calculate_next_step(self.surface, self.mesh_height, self.mesh_width, border_condition)

        # self.wid.canvas.clear()
        # self.create_mesh()

        keep_going = False
        for i in range(self.mesh_height):
            for j in range(self.mesh_width):
                if self.surface[i][j].id is not 0 and self.surface[i][j].drawn is False:
                    self.draw_point(self.surface[i][j].x, self.surface[i][j].y, self.surface[i][j].color)
                    self.surface[i][j].drawn = True
                else:
                    keep_going = True

        return keep_going


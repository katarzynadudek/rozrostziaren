import threading

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.widget import Widget

from Expansion.User import User
from Utility.Drawing import Drawing
from Expansion.Radius import Radius
from Expansion.RandomCase import RandomCase
from Expansion.Uniform import Uniform


class Board(Widget):

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)
        self.bind(on_touch_down=self.touch_down)

        self.register_event_type('on_test')

    def touch_down(self, _, touch):
        self.dispatch('on_test', touch)
        print(touch)

    def on_test(self, touch):
        pass


class NewClass(FloatLayout):

    def __init__(self, **kwargs):
        super(NewClass, self).__init__(**kwargs)

        self.speed = 1.0
        self.mesh_height = int(1)
        self.mesh_width = int(1)
        self.callback_cout = 1
        self.animation_length = 100
        self.border_condition = False

        self.radiusLabel = None
        self.rowLabel = None
        self.randomAmountLabel = None

        Window.size = (800, 650)

        uniformButton = Button(text='Jednorodne', size_hint_x=None, size_hint=(.15, .09),
                               background_color=(1, 0, 0, 1), pos_hint={'x': 0.05, 'y': 1.85})
        uniformButton.bind(on_press=lambda x: self.jednorodne())
        self.add_widget(uniformButton)

        radiusButton = Button(text='Z promieniem', size_hint_x=None, size_hint=(.15, .09),
                              background_color=(1, 0, 0, 1), pos_hint={'x': 0.2, 'y': 1.85})
        radiusButton.bind(on_press=lambda x: self.z_promieniem())
        self.add_widget(radiusButton)

        randomButton = Button(text='Losowe', size_hint_x=None, size_hint=(.15, .09),
                              background_color=(1, 0, 0, 1), pos_hint={'x': 0.35, 'y': 1.85})
        randomButton.bind(on_press=lambda x: self.losowe())
        self.add_widget(randomButton)

        userInputButton = Button(text='RESET', size_hint_x=None, size_hint=(.15, .09),
                                 pos_hint={'x': 0.52, 'y': 1.85})
        userInputButton.bind(on_press=lambda x: self.reset())
        self.add_widget(userInputButton)

        label = Label(text='Rozmiar siatki: ', pos_hint={'x': 0.1, 'y': .1}, size_hint=(.15, .09))
        self.add_widget(label)

        self.heightInput = TextInput(text='', size_hint=(.05, .09), pos_hint={'x': 0.25, 'y': .1}, multiline=False)
        self.heightInput.bind(text=lambda x, y: self.save_width(mesh_width=self.heightInput.text))
        self.add_widget(self.heightInput)

        self.widthInput = TextInput(text='', size_hint=(.05, .09), pos_hint={'x': 0.3, 'y': .1}, multiline=False)
        self.widthInput.bind(text=lambda x, y: self.save_height(mesh_height=self.widthInput.text))
        self.add_widget(self.widthInput)

        animation = Button(text='START', size_hint=(.15, .09),
                           background_color=(1, 0, 0, 1), pos_hint={'x': 0.75, 'y': .1})
        animation.bind(on_press=self.call_animation)
        self.add_widget(animation)

        btn1 = ToggleButton(text='absorbujące', group='border_condition', state='down', size_hint=(.15, .09),
                            pos_hint={'x': 0.4, 'y': .1})
        btn1.bind(on_press=lambda x: self.save_border_condition(state=False))
        self.add_widget(btn1)
        btn2 = ToggleButton(text='perdiodyczne', group='border_condition', size_hint=(.15, .09),
                            pos_hint={'x': 0.55, 'y': .1})
        btn2.bind(on_press=lambda x: self.save_border_condition(state=True))
        self.add_widget(btn2)

        board = Board(pos_hint={'x': .5, 'y': .5}, size_hint=(.2, .1))
        board.bind(on_test=self.on_board_event)
        self.add_widget(board)

    def reset(self):

        if hasattr(self, "nr_of_embryos"):
            self.remove_widget(self.nr_of_embryos)
        if self.radiusLabel is not None:
            self.remove_widget(self.radiusLabel)
            self.remove_widget(self.radiusAmount)
            self.remove_widget(self.amountLabel)
            self.remove_widget(self.amount)
        if self.randomAmountLabel is not None:
            self.remove_widget(self.randomAmountLabel)
            self.remove_widget(self.randomAmount)
        if self.rowLabel is not None:
            self.remove_widget(self.rowLabel)
            self.remove_widget(self.rowAmount)
            self.remove_widget(self.columnLabel)
            self.remove_widget(self.columnAmount)

        self.cancel_animation()

        self.__delattr__('drawing')
        wid.canvas.clear()
        self.heightInput.text = ''
        self.widthInput.text = ''

    def on_board_event(self, _, touch):

        if hasattr(self, 'drawing'):
            User.create_user_input(mesh_height=self.mesh_height, mesh_width=self.mesh_width, touch=touch,
                                   drawing=self.drawing)

    def save_border_condition(self, state):
        self.border_condition = state

    def jednorodne(self):
        print(self)

        if self.radiusLabel is not None:
            self.remove_widget(self.radiusLabel)
            self.remove_widget(self.radiusAmount)
            self.remove_widget(self.amountLabel)
            self.remove_widget(self.amount)
        if self.randomAmountLabel is not None:
            self.remove_widget(self.randomAmountLabel)
            self.remove_widget(self.randomAmount)
        if hasattr(self, "nr_of_embryos"):
            self.remove_widget(self.nr_of_embryos)

        self.rowLabel = Label(text="wiersz", pos_hint={'x': 0.68, 'y': 1.85}, size_hint_x=None, size_hint=(.05, .09),
                              id="rowLabel")
        self.add_widget(self.rowLabel)

        self.rowAmount = TextInput(size_hint_x=None, size_hint=(.05, .09), pos_hint={'x': 0.75, 'y': 1.85},
                                   multiline=False)
        self.add_widget(self.rowAmount)

        self.columnLabel = Label(text="kolumna", pos_hint={'x': 0.82, 'y': 1.85}, size_hint_x=None,
                                 size_hint=(.05, .09))
        self.add_widget(self.columnLabel)
        self.columnAmount = TextInput(size_hint_x=None, size_hint=(.05, .09), pos_hint={'x': 0.9, 'y': 1.85},
                                      multiline=False)
        self.columnAmount.bind(
            on_text_validate=lambda x: Uniform.create_uniform(drawing=self.drawing, row=self.rowAmount.text,
                                                              column=self.columnAmount.text,
                                                              mesh_height=self.mesh_height,
                                                              mesh_width=self.mesh_width))
        self.add_widget(self.columnAmount)

    def z_promieniem(self):

        if self.rowLabel is not None:
            self.remove_widget(self.rowLabel)
            self.remove_widget(self.rowAmount)
            self.remove_widget(self.columnLabel)
            self.remove_widget(self.columnAmount)
        if self.randomAmountLabel is not None:
            self.remove_widget(self.randomAmountLabel)
            self.remove_widget(self.randomAmount)
        if hasattr(self, "nr_of_embryos"):
            self.remove_widget(self.nr_of_embryos)

        self.radiusLabel = Label(text="promien", pos_hint={'x': 0.68, 'y': 1.85}, size_hint_x=None,
                                 size_hint=(.05, .09))
        self.add_widget(self.radiusLabel)
        self.radiusAmount = TextInput(size_hint_x=None, size_hint=(.05, .09), pos_hint={'x': 0.75, 'y': 1.85},
                                      multiline=False)
        self.add_widget(self.radiusAmount)

        self.amountLabel = Label(text="ilosc", pos_hint={'x': 0.82, 'y': 1.85}, size_hint_x=None, size_hint=(.05, .09))
        self.add_widget(self.amountLabel)
        self.amount = TextInput(size_hint_x=None, size_hint=(.05, .09), pos_hint={'x': 0.9, 'y': 1.85}, multiline=False)
        self.amount.bind(
            on_text_validate=lambda x: Radius.create_radius(newclass=self,
                                                            drawing=self.drawing, radius=self.radiusAmount.text,
                                                            amount=self.amount.text, mesh_width=self.mesh_width,
                                                            mesh_height=self.mesh_height))
        self.add_widget(self.amount)

    def print_no_embryos(self, nr):
        print('here', nr)
        self.nr_of_embryos = Label(text="Znaleziono:" + str(nr), pos_hint={'x': 0.8, 'y': 1.6}, size_hint_x=None,
                                   size_hint=(.05, .09))
        self.add_widget(self.nr_of_embryos)

    def losowe(self):

        if self.rowLabel is not None:
            self.remove_widget(self.rowLabel)
            self.remove_widget(self.rowAmount)
            self.remove_widget(self.columnLabel)
            self.remove_widget(self.columnAmount)
        if self.radiusLabel is not None:
            self.remove_widget(self.radiusLabel)
            self.remove_widget(self.radiusAmount)
            self.remove_widget(self.amountLabel)
            self.remove_widget(self.amount)
        if hasattr(self, "nr_of_embryos"):
            self.remove_widget(self.nr_of_embryos)

        self.randomAmountLabel = Label(text="ilosc", pos_hint={'x': 0.68, 'y': 1.85}, size_hint_x=None,
                                       size_hint=(.05, .09))
        self.add_widget(self.randomAmountLabel)
        self.randomAmount = TextInput(size_hint_x=None, size_hint=(.05, .09), pos_hint={'x': 0.75, 'y': 1.85},
                                      multiline=False)
        self.randomAmount.bind(
            on_text_validate=lambda x: RandomCase.create_random(drawing=self.drawing, amount=self.randomAmount.text,
                                                                mesh_width=self.mesh_width,
                                                                mesh_height=self.mesh_height))
        self.add_widget(self.randomAmount)

    def call_animation(self, button):

        if (button.text == 'START'):
            self.animate()
            button.text = 'STOP'
        else:
            self.cancel_animation()
            button.text = 'START'

    def animate(self, *args):

        # keep_going = self.drawing.draw_animation()

        self.t = threading.Timer(self.speed, self.animate)
        self.t.start()

        keep_going = self.drawing.draw_animation(self.border_condition)

        if not keep_going:
            self.cancel_animation()

    def cancel_animation(self):
        if hasattr(self, 't'):
            self.t.cancel()

    def save_width(self, mesh_width):
        if mesh_width != '' and int(mesh_width) >= 0:
            self.mesh_width = int(mesh_width)
            # create_mesh(self.mesh_width, self.mesh_height)

    def save_height(self, mesh_height):
        if mesh_height != '' and int(mesh_height) >= 0:
            self.mesh_height = int(mesh_height)

            self.drawing = Drawing(wid, self.mesh_width, self.mesh_height)
            self.drawing.create_mesh()


class RozrostZiarenApp(App):

    def build(self):
        root = BoxLayout(orientation='vertical')
        root.add_widget(wid)
        root.add_widget(NewClass())
        return root


if __name__ == '__main__':
    wid = Widget()
    RozrostZiarenApp().run()
